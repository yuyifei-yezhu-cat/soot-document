package com.yuyifei;

import java.util.Iterator;
import java.util.Map;

import soot.Body;
import soot.BodyTransformer;
import soot.Local;
import soot.PatchingChain;
import soot.SootClass;
import soot.Trap;
import soot.Unit;
import soot.util.Chain;

public class GotoInstrumenter extends BodyTransformer{

	@Override
	protected void internalTransform(Body body, String arg1, Map<String, String> arg2)
	{
		//print the class name
		SootClass sClass = body.getMethod().getDeclaringClass();
		System.out.println("***************" + sClass.getName());
		
		//print the units
		final PatchingChain<Unit> units = body.getUnits();
		Iterator<Unit> us = units.snapshotIterator();
		while (us.hasNext()) 
		{
			Unit u = us.next();
			System.out.println("-------------->" + u.toString());
			
			/*
			 * test getUseBoxes(), getDefBoxes(), getUseAndDefBoxes()
			 * getBoxesPointingToThis(), getUnitBoxes()
			 */
		}
		
		//print the Locals
		Chain<Local> locals = body.getLocals();
		Iterator<Local> ls = locals.snapshotIterator();
		while (ls.hasNext()) 
		{
			Local l = ls.next();
			System.out.println("+++++++++++++" + l.toString());
		}
		
		//print the Traps
		Chain<Trap> traps = body.getTraps();
		Iterator<Trap> ts = traps.snapshotIterator();
		while (ts.hasNext()) 
		{
			Trap t = ts.next();
			System.out.println("&&&&&&&&&&&&&" + t.toString());
		}
	}

}
