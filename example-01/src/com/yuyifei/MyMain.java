package com.yuyifei;
/* Soot - a J*va Optimization Framework
 * Copyright (C) 1997-1999 Raja Vallee-Rai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the Sable Research Group and others 1997-1999.  
 * See the 'credits' file distributed with Soot for the complete list of
 * contributors.  (Soot is distributed at http://www.sable.mcgill.ca/soot)
 */

import soot.*;
import soot.options.Options;

/** 
   Example to instrument a classfile to produce goto counts. 
 */
public class MyMain
{    
    public static void main(String[] args) 
    {        
        String[] args1 = {
        		"-cp",
        		"/home/yuyifei/Downloads/yuyifei/example-01/src/",
        		"-pp",
        		"-src-prec",
        		"java",
        		"com.yuyifei.GoTo"
        };
        
        //Options.v().set_output_format(Options.output_format_class);
        //Options.v().set_output_format(Options.output_format_jimple);
        
    	PackManager.v().getPack("jtp").add(new Transform("jtp.instrumenter", new GotoInstrumenter()));
        
        // Just in case, resolve the PrintStream and System soot-classes
        Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
        Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);
        soot.Main.main(args1);
    }
}