#1 soot简介

soot起初的出发点是实现一个java代码优化框架，然而发展至今，经过众多学者的扩展，
soot已被广泛用于分析、插桩、优化java和Android应用。（use Soot to analyze, 
instrument, optimize and visualize Java and Android applications.）

soot可以处理的文件类型

+ Java (bytecode and source code up to Java 7), 
  including other languages that compile to Java bytecode, e.g. Scala
+ Android bytecode
+ Jimple intermediate representation (see below)
+ Jasmin, a low-level intermediate representation

soot的输出文件类型

+ Java bytecode（.class file）
+ Android bytecode (eg .dex file and apk file)
+ Jimple (soot IR file)
+ Jasmin

#2 Basic Soot Constructs

+ Scene
+ SootClass
+ SootMethod
+ SootField
+ Body
    * BafBody
    * GrimpBody
    * ShimpleBody
    * JimpleBody

每个body又由以下几个数据结构组成

+ Units 语句
+ Locals局部变量
+ traps 异常处理

他们是一个链状结构，通过以下几个方法可以得到这个链：

+ body.getUnits();
+ body.getLocals()
+ body.getTraps()

示例1：example-01

运行MyMain.java则会在sootOutput目录下生成文件。

In addition:

    getUseBoxes(), getDefBoxes(), getUseAndDefBoxes(), getBoxesPointingToThis(), getUnitBoxes() 

#3 Intermediate Representations

+ Baf
+ Jimple
+ Shimple
+ Grimp

下面主要分析一下Jimple:

Jimple is the principal representation in Soot. The Jimple representation is a
typed, 3-address, statement based intermediate representation.

Jimple总共有15条statements：

+ the core statements
+ Statements for intraprocedural control-flow
+ .....

    public class Foo {
        public static void main(String[] args) {
            Foo f = new Foo();
            int a = 7;
            int b = 14;
            int x = (f.bar(21) + a) * b;
        }
        public int bar(int n) { return n + 42; }
    }

相应的Jimple代码：

    public static void main(java.lang.String[]) {
        java.lang.String[] r0;
        Foo $r1, r2;
        int i0, i1, i2, $i3, $i4;
        r0 := @parameter0: java.lang.String[];
        $r1 = new Foo;
        specialinvoke $r1.<Foo: void <init>()>();
        r2 = $r1;
        i0 = 7;
        i1 = 14;
        // InvokeStmt
        $i3 = virtualinvoke r2.<Foo: int bar()>(21);
        $i4 = $i3 + i0;
        i2 = $i4 * i1;
        return;
    }
    
    public int bar() {
        Foo r0;
        int i0, $i1;
        r0 := @this: Foo; // IdentityStmt
        i0 := @parameter0: int; // IdentityStmt
        $i1 = i0 + 21; // AssignStmt
        return \$i1; // ReturnStmt
    }
    
#4 Soot as a stand-alone tool

soot官网提供了许多小例子：

文档链接：https://github.com/Sable/soot/wiki/Tutorials

## 4.1 Creating a class from scratch with soot

代码：example-02

该例子已经集成在soot插件中，可以自动生成：

    File--->New--->Example--->Creating a class from scratch with soot--->next--->example-02--->finish

主要创建过程：

    /** Example of using Soot to create a classfile from scratch and annotate it.
     * The example proceeds as follows:
     *
     * - Create a SootClass <code>HelloWorld</code> extending java.lang.Object.
     *
     * - Create a 'main' method and add it to the class.
     *
     * - Create an empty JimpleBody and add it to the 'main' method.
     *
     * - Add locals and statements to JimpleBody.
     * 
     * - Add annotations.
     *
     * - Write the result out to a class file.
     */
     

##4.2 Extending Soot’s Main class

一个简单的扩展示例：

代码：example-03

##4.3 java代码instrument

代码示例：example-04

##4.3 Android apk instrument

前提条件：

1、下载最新版本的soot:soot-trunk.jar

    https://ssebuild.cased.de/nightly/soot/
    
2、下载各个版本的Android.jar

    https://github.com/Sable/android-platforms
    
3、实现一个apk文件
    
    RV2013项目
    
实现主要代码：

+ 设置命令行选项：

        String[] args1 = {
            "-android-jars",
            "/home/yuyifei/Desktop/android-jar/android-platforms-master",
            "-allow-phantom-refs",
            "-ire",
            "-process-dir",
            //"/home/yuyifei/Desktop/apk-test/SkeletonActivity.apk"
            "/home/yuyifei/Desktop/githhub-paper/android-instrumentation-tutorial/app-example/RV2013/bin/RV2013.apk"
        };

+ 输出格式设置： 

        //prefer Android APK files// -src-prec apk
        Options.v().set_src_prec(Options.src_prec_apk);
		
        //output as APK, too//-f J
        Options.v().set_output_format(Options.output_format_dex);

+ 重写BodyTransformer:

        //resolve the PrintStream and System soot-classes
    	Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
        Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);
        PackManager.v().getPack("jtp").add(new Transform("jtp.myInstrumenter", new MyBodyTransformer()));	
        PackManager.v().runPacks();
        PackManager.v().writeOutput();
        //soot.Main.main(args);
        soot.Main.main(args1);

+ 重写的BodyTransformer类MyBodyTransformer：

    
        class MyBodyTransformer extends BodyTransformer{
        
            @Override
        	protected void internalTransform(Body body, String arg0, Map arg1) {
        		System.out.println("Hello");
        		if (true)
        		{
        			System.out.println("yuyifei");
        			return;
        		}
        		if (body.getMethod().getDeclaringClass().getName().startsWith("de.ecspride")) {
        			Iterator<Unit> i = body.getUnits().snapshotIterator();
        			while (i.hasNext()) {
        				Unit u = i.next();
        				if(u.toString().contains("println")){
        	//				removeStatement(u, body);				
        					replaceStatementByLog(u, body);
        				}
        				eliminatePremiumRateSMS(u, body);
        			}
        		}
        	}
        	
        	private void removeStatement(Unit u, Body body){
        			body.getUnits().remove(u);
        	}
        	
        	private void replaceStatementByLog(Unit u, Body body){
        		SootMethod sm = Scene.v().getMethod("<android.util.Log: int i(java.lang.String,java.lang.String)>");
        		Value logType = StringConstant.v("INFO");
        		Value logMessage = StringConstant.v("yuyifei: replaced log information");
        		
        		StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(sm.makeRef(), logType, logMessage);
        		Unit generated = Jimple.v().newInvokeStmt(invokeExpr);
        		
        		body.getUnits().insertAfter(generated, u);
        		
        		body.getUnits().remove(u);
        	}
        	
        	private void eliminatePremiumRateSMS(Unit u, Body body){		
        		if(u instanceof InvokeStmt){
        			InvokeStmt invoke = (InvokeStmt)u;
        			
        			if(invoke.getInvokeExpr().getMethod().getSignature().equals("<android.telephony.SmsManager: void sendTextMessage(java.lang.String,java.lang.String,java.lang.String,android.app.PendingIntent,android.app.PendingIntent)>")){
        				Value phoneNumber = invoke.getInvokeExpr().getArg(0);
        				
        				if(phoneNumber instanceof StringConstant){
        					removeStatement(u, body);
        				}
        				else if(body.getLocals().contains(phoneNumber)){
        					Local phoneNumberLocal = (Local)phoneNumber;
        					List<Unit> generated = new ArrayList<Unit>();
        					
        					//generate startsWith method
        					VirtualInvokeExpr vinvokeExpr = generateStartsWithMethod(body, phoneNumberLocal);
        					
        					//generate assignment of local (boolean) with the startsWith method
        					Type booleanType = BooleanType.v();
        					Local localBoolean = generateNewLocal(body, booleanType);
        					AssignStmt astmt = Jimple.v().newAssignStmt(localBoolean, vinvokeExpr);
        					generated.add(astmt);
        					
        					//generate condition
        					IntConstant zero = IntConstant.v(0);
        					EqExpr equalExpr = Jimple.v().newEqExpr(localBoolean, zero);
        					NopStmt nop = insertNopStmt(body, u);
        					IfStmt ifStmt = Jimple.v().newIfStmt(equalExpr, nop);
        					generated.add(ifStmt);
        					
        					body.getUnits().insertBefore(generated, u);
        				}
        			}
        				
        		}
        	}
        	
        	private VirtualInvokeExpr generateStartsWithMethod(Body body, Local phoneNumberLocal){
        		SootMethod sm = Scene.v().getMethod("<java.lang.String: boolean startsWith(java.lang.String)>");
        		
        		
        		Value value = StringConstant.v("0900");
        		VirtualInvokeExpr vinvokeExpr = Jimple.v().newVirtualInvokeExpr(phoneNumberLocal, sm.makeRef(), value);
        		return vinvokeExpr;
        	}
        	
        	private Local generateNewLocal(Body body, Type type){
        		LocalGenerator lg = new LocalGenerator(body);
        		return lg.generateLocal(type);
        	}	
        	
        	private NopStmt insertNopStmt(Body body, Unit u){
        		NopStmt nop = Jimple.v().newNopStmt();
        		body.getUnits().insertAfter(nop, u);
        		return nop;
        	}
        }
        

代码示例：apk-instrument项目

整个过程:

* 生成apk文件
* 运行instrument代码
* 对apk文件进行签名
* 对apk文件进行对其处理
* 安装apk文件

参考文献：
    https://github.com/Sable/soot/wiki/Instrumenting-Android-Apps-with-Soot

#注意事项

1、soot插件在高版本的eclipse安装可能有问题
2、soot-2.5.0 不能在jdk1.8上运行
3、jdk最好安装1.7版本
4、instrument apk时以前有bug，需要这样写：

    protected void internalTransform(final Body body, String phaseName, @SuppressWarnings("rawtypes") Map options)

