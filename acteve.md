# acteve项目

基于符号执行的Android应用自动化测试工具

命令行启动/关闭一个Android应用：

    adb shell am start/stop -n package/mainActivity
    eg.  adb shell am start -n de.ecspride/.RV2013

## monkey简介

Monkey是Android中的一个命令行工具，可以运行在模拟器里或实际设备中。它向系统发送伪随机
的用户事件流(如按键输入、触摸屏输入、手势输入等)，实现对正在开发的应用程序进行压力测试。
Monkey测试是一种为了测试软件的稳定性、健壮性的快速有效的方法。
官方文档：

    http://developer.android.com/tools/help/monkey.html

1、Android命令行

运行Anndroid模拟器后，使用在linux终端下执行 adb shell 即可进入Android模拟器的命令行执行
界面。

2、monkey的特性

* 测试的对象仅为应用程序包，有一定的局限性。
* Monky测试使用的事件流数据流是随机的，不能进行自定义。
* 可对MonkeyTest的对象，事件数量，类型，频率等进行设置。

3、monkey基本语法

    adb shell monkey [options]
    
如果不指定options，Monkey将以无反馈模式启动，并把事件任意发送到安装在目标环境中的
全部包。下面是一个更为典型的命令行示例，它启动指定的应用程序，并向其发送500个伪随
机事件：

    adb shell monkey [options] <event-count>
    eg. adb shell monkey -p de.ecspride -v 500
    
    -p 指定package
    -v 指定打印信息的详细级别，一个 -v增加一个级别 ， 默认级别为 0 
    event-count 指产生随机事件数量
