package com.yuyifei;

import java.util.Iterator;
import java.util.Map;
import soot.Body;
import soot.BodyTransformer;
import soot.Local;
import soot.PackManager;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Transform;
import soot.Unit;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.StringConstant;
import soot.options.Options;
import java.util.ArrayList;
import java.util.List;
import soot.BooleanType;
import soot.Type;
import soot.Value;
import soot.javaToJimple.LocalGenerator;
import soot.jimple.AssignStmt;
import soot.jimple.EqExpr;
import soot.jimple.IfStmt;
import soot.jimple.IntConstant;
import soot.jimple.NopStmt;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.VirtualInvokeExpr;


public class InstrumentMain {
	
	public static void main(String[] args) {
		String[] args1 = {
				"-android-jars",
				"/home/yuyifei/Desktop/android-jar/android-platforms-master",
				"-allow-phantom-refs",
				"-ire",
				"-process-dir",
				//"/home/yuyifei/Desktop/apk-test/SkeletonActivity.apk"
				"/home/yuyifei/Desktop/githhub-paper/android-instrumentation-tutorial/app-example/RV2013/bin/RV2013.apk"
		};
		
		//prefer Android APK files// -src-prec apk
		Options.v().set_src_prec(Options.src_prec_apk);
		
		//output as APK, too//-f J
		Options.v().set_output_format(Options.output_format_dex);
		
        // resolve the PrintStream and System soot-classes
		Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
        Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);

        PackManager.v().getPack("jtp").add(new Transform("jtp.myInstrumenter", new MyBodyTransformer()));
		
        PackManager.v().runPacks();
        PackManager.v().writeOutput();
		//soot.Main.main(args);
        soot.Main.main(args1);
	}
}


class MyBodyTransformer extends BodyTransformer{

	@Override
	protected void internalTransform(Body body, String arg0, Map arg1) {
		System.out.println("Hello");
		if (true)
		{
			System.out.println("yuyifei");
			return;
		}
		if (body.getMethod().getDeclaringClass().getName().startsWith("de.ecspride")) {
			Iterator<Unit> i = body.getUnits().snapshotIterator();
			while (i.hasNext()) {
				Unit u = i.next();
				if(u.toString().contains("println")){
	//				removeStatement(u, body);				
					replaceStatementByLog(u, body);
				}
				eliminatePremiumRateSMS(u, body);
			}
		}
	}
	
	private void removeStatement(Unit u, Body body){
			body.getUnits().remove(u);
	}
	
	private void replaceStatementByLog(Unit u, Body body){
		SootMethod sm = Scene.v().getMethod("<android.util.Log: int i(java.lang.String,java.lang.String)>");
		Value logType = StringConstant.v("INFO");
		Value logMessage = StringConstant.v("yuyifei: replaced log information");
		
		StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(sm.makeRef(), logType, logMessage);
		Unit generated = Jimple.v().newInvokeStmt(invokeExpr);
		
		body.getUnits().insertAfter(generated, u);
		
		body.getUnits().remove(u);
	}
	
	private void eliminatePremiumRateSMS(Unit u, Body body){		
		if(u instanceof InvokeStmt){
			InvokeStmt invoke = (InvokeStmt)u;
			
			if(invoke.getInvokeExpr().getMethod().getSignature().equals("<android.telephony.SmsManager: void sendTextMessage(java.lang.String,java.lang.String,java.lang.String,android.app.PendingIntent,android.app.PendingIntent)>")){
				Value phoneNumber = invoke.getInvokeExpr().getArg(0);
				
				if(phoneNumber instanceof StringConstant){
					removeStatement(u, body);
				}
				else if(body.getLocals().contains(phoneNumber)){
					Local phoneNumberLocal = (Local)phoneNumber;
					List<Unit> generated = new ArrayList<Unit>();
					
					//generate startsWith method
					VirtualInvokeExpr vinvokeExpr = generateStartsWithMethod(body, phoneNumberLocal);
					
					//generate assignment of local (boolean) with the startsWith method
					Type booleanType = BooleanType.v();
					Local localBoolean = generateNewLocal(body, booleanType);
					AssignStmt astmt = Jimple.v().newAssignStmt(localBoolean, vinvokeExpr);
					generated.add(astmt);
					
					//generate condition
					IntConstant zero = IntConstant.v(0);
					EqExpr equalExpr = Jimple.v().newEqExpr(localBoolean, zero);
					NopStmt nop = insertNopStmt(body, u);
					IfStmt ifStmt = Jimple.v().newIfStmt(equalExpr, nop);
					generated.add(ifStmt);
					
					body.getUnits().insertBefore(generated, u);
				}
			}
				
		}
	}
	
	private VirtualInvokeExpr generateStartsWithMethod(Body body, Local phoneNumberLocal){
		SootMethod sm = Scene.v().getMethod("<java.lang.String: boolean startsWith(java.lang.String)>");
		
		
		Value value = StringConstant.v("0900");
		VirtualInvokeExpr vinvokeExpr = Jimple.v().newVirtualInvokeExpr(phoneNumberLocal, sm.makeRef(), value);
		return vinvokeExpr;
	}
	
	private Local generateNewLocal(Body body, Type type){
		LocalGenerator lg = new LocalGenerator(body);
		return lg.generateLocal(type);
	}	
	
	private NopStmt insertNopStmt(Body body, Unit u){
		NopStmt nop = Jimple.v().newNopStmt();
		body.getUnits().insertAfter(nop, u);
		return nop;
	}
}
