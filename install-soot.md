##1.1 Download url:
 

    https://ssebuild.cased.de/nightly/soot/

 运行soot：

    java -cp soot-trunk.jar soot.Main --help

运行后将会得到soot的许多命令行选项，各个选项的具体函数可以参照以下文档：

    文档链接：https://ssebuild.cased.de/nightly/soot/doc/soot_options.htm
    
##1.2 Developing with Soot in Eclipse

soot除了是一个工具外，我们还可以利用他提供的jar进行开发。
在自己创建的java工程中使用soot提供的api需要将soot的jar导入到工程中。
假设已经建立了一个新的java项目，我们可以通过以下步骤导入soot jar包：

    右键点击项目--->properties--->Java Build Path--->add external JARs--->找到该jars--->ok---->ok

##1.3 Setting up the Soot Eclipse plugin

soot除了可以自己输入命令行之外，还提供了一个eclipse插件。值得注意的是目前版本的soot只支持
Eclipse Kepler版本的eclipse，不支持Eclipse Luna版本的eclipse。

    NOTE: The Eclipse plugin is currently not running under Eclipse Luna, we recommend you to use Eclipse Kepler to use it.
    https://github.com/Sable/soot/wiki/Running-Soot-as-Eclipse-Plugin


eclipse插件的安装步骤：

    Help(eclipse上面的按钮)--->Install New SoftWare--->add--->Name(eg. soot)Location(eg. http://www.sable.mcgill.ca/soot/eclipse/updates/)--->ok---> ...

soot插件安装成功后，随意右击一个java文件，在弹出的菜单中会出现soot按钮。

